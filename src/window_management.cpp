#include "window_management.hpp"

class CompizReloaded : public IWindowManager
{
private:
    DBus dbus;

public:
    CompizReloaded() : dbus(BUS_NAME, BUS_PATH, BUS_IFACE){};

    bool init();

    bool deinit();

    bool open();

    bool kill();

    bool hide();
};
