#include "brightness.hpp"

bool Brightness::brightnessUp()
{
    return dbus.genericMethodCall("StepUp", NULL);
}

bool Brightness::brightnessDown()
{
    return dbus.genericMethodCall("StepDown", NULL);
}

int Brightness::read()
{
    return dbus.getIntProp(BUS_IFACE, "Brightness");
}