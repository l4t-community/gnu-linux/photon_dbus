#include "desktop_files.hpp"

std::vector<DesktopFilesList> DesktopFiles::fetch()
{
    GList *iter;
    GList *apps = g_app_info_get_all();

    std::vector<DesktopFilesList> ret;

    /* Populate appBox */
    for (iter = apps; iter; iter = iter->next)
    {
        DesktopFilesList constructedApp;
        GAppInfo *app = G_APP_INFO(iter->data);

        /* Skip apps that should not be shown or NULL */
        if (app == NULL)
            continue;
        if (!g_app_info_should_show(app))
            continue;

        /* Parse icon using GTK helpers to get path */
        GtkIconInfo *icon;
        GIcon *g_icon = g_app_info_get_icon(app);

        if (g_icon)
            icon = gtk_icon_theme_lookup_by_gicon_for_scale(
                gtk_icon_theme_get_default(),
                g_icon,
                256,
                1,
                GtkIconLookupFlags(GTK_ICON_LOOKUP_FORCE_SIZE));

        if (icon)
            constructedApp.iconPath = gtk_icon_info_get_filename(icon);

        constructedApp.name = g_app_info_get_display_name(app);
        constructedApp.ref = app;

        /* Push app retrieved to vector */
        ret.push_back(constructedApp);
    }

    return ret;
}

bool DesktopFiles::launch(GAppInfo *app)
{
    GError *error = NULL;
    GAppLaunchContext *ctx = g_app_launch_context_new();

    return g_app_info_launch(app, NULL, ctx, &error);
}
