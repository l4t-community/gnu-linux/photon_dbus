#include "upower.hpp"

bool UPower::isOnBattery()
{
    dbus.objectPath = "/org/freedesktop/UPower";
    return dbus.getBoolProp("org.freedesktop.UPower", "OnBattery");
}

const gchar *UPower::get_battery_path()
{
    return dbus.findDevice("bat");
}

int UPower::percentage()
{
    dbus.interface = get_battery_path();
    return dbus.getIntProp("org.freedesktop.UPower.Device", "Percentage");
}