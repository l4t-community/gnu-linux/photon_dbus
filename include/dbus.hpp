#include <array>
#include <map>
#include <iostream>
#include <sstream>
#include <string>

#include <gio/gio.h>

#pragma once

class DBus
{
    public:
        GBusType busType;
        const gchar *busName, *objectPath, *interface;

        DBus(const gchar *name,
            const gchar *path,
            const gchar *iface,
            GBusType type = G_BUS_TYPE_SYSTEM)
        {
            if (type) busType = type;
            busName = name;
            objectPath = path;
            interface = iface;
        };

        /* Setters */
        bool setProp(const char *adapter, const char *prop, GVariant *value);

        /* Getters */
        GVariant *getProp(const gchar *adapter, const char *prop);
        const gchar *getStrProp(const gchar *adapter, const char *prop);
        bool getBoolProp(const gchar *adapter, const char *prop);
        int getIntProp(const gchar *adapter, const char *prop);

        /* Helpers */
        gchar *findDevice(const gchar *pattern);

        /* Proxys */
        bool genericMethodCall(const gchar *method, GVariant *param);
        GDBusProxy *createProxy();
        GVariant *methodCall(const gchar *method, GVariant *param);
};